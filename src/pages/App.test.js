import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { renderHook, act } from '@testing-library/react-hooks';
import { useLocation } from 'wouter';
import App, { Dragger, compList } from './App';

describe('App component tests', () => {
  const { result, unmount } = renderHook(() => useLocation());
  const setLocation = result.current[1];

  it('Dragger component test', async () => {
    let currentPage = compList[0];
    let prevPage = compList[1];

    const { rerender } = render(
      <Dragger cmList={compList} currentPage={currentPage} prevPage={prevPage} />
    );
    const tb = await screen.findByTestId('animated-route');
    expect(tb).toHaveClass('pl');

    currentPage = compList[1];
    prevPage = compList[0];
    rerender(
      <Dragger cmList={compList} currentPage={currentPage} prevPage={prevPage} />
    );
    expect(tb).toHaveClass('pr');

    currentPage = compList[2];
    prevPage = undefined;
    rerender(
      <Dragger cmList={compList} currentPage={currentPage} prevPage={prevPage} />
    );
    expect(tb).toHaveClass('pr');
  });

  it('Dragger 404 test', async () => {
    const currentPage = undefined;
    const prevPage = compList[3];
    render(
      <Dragger cmList={compList} currentPage={currentPage} prevPage={prevPage} />
    );
    const er = await screen.queryByTestId('animated-route');
    expect(er).toBeNull();
  });

  it('App page not found routing test', async () => {
    act(() => setLocation('/saadsa'));
    render(
      <App />
    );
    const er = await screen.queryByTestId('error-page');
    expect(er).toHaveTextContent('Not Found');
    unmount();
  });

  it('App page routing test', async () => {
    act(() => setLocation('/'));
    render(
      <App />
    );
    const er = await screen.queryByTestId('animated-route');
    expect(er).toBeInTheDocument();
    unmount();
  });
});
