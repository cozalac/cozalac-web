import '@testing-library/jest-dom';
import React from 'react';
import { gql } from '@apollo/client';
import { MockedProvider } from '@apollo/client/testing';
import { render, screen } from '@testing-library/react';
import Contact, { GET_CONTACT } from './Contact';

describe('Contact component test', () => {
  it('Contact test', async () => {
    const testText = 'Test content sahdhgas fasjdh sdjfhs.';
    const mocks = [
      {
        request: {
          query: GET_CONTACT,
        },
        result: {
          data: {
            pages: [ { Name: 'contact', Content: { BodyText: testText }, } ],
          },
        },
      },
    ];

    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Contact />
      </MockedProvider>
    );

    //expect(screen.queryByText('Loading')).toBeInTheDocument();

    const content = await screen.findByText(testText);
    expect(content).toHaveTextContent(testText);
  });
});
