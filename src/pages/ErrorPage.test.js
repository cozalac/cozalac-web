import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import ErrorPage from './ErrorPage';

describe('ErrorPage component test', () => {
  it('Page test', () => {
    const route = { code: '404' };
    const errorTexts = {
      403: 'Forbidden',
      404: 'Not Found',
    };

    const { rerender } = render(<ErrorPage params={route} />);
    expect(screen.queryByText(errorTexts[route.code])).toBeInTheDocument();

    route.code = '403';
    rerender(<ErrorPage params={route} />);
    expect(screen.queryByText(errorTexts[route.code])).toBeInTheDocument();
  });
});
