import '@testing-library/jest-dom';
import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { render, screen } from '@testing-library/react';
import About, { GET_ABOUT } from './About';

describe('About component test', () => {
  it('About test', async () => {
    const testTitle = 'Bu bir basliktir';
    const testText = 'Test content sahdhgas fasjdh sdjfhs.';
    const testUrl = '/git/yinegit.jpg';
    const mocks = [
      {
        request: {
          query: GET_ABOUT,
          variables: {
            name: 'about'
          },
        },
        result: {
          data: {
            pages: [ { Name: 'about', Content: { Title: testTitle, BodyText: testText, Image: { url: testUrl }, }, } ],
          },
        },
      },
    ];

    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <About />
      </MockedProvider>
    );

    //expect(screen.queryByText('Loading')).toBeInTheDocument();

    const text = await screen.findByText(testText);
    const title = await screen.findByText(testTitle);
    const image = await screen.findByTestId('about-image');
    expect(text).toHaveTextContent(testText);
    expect(title).toHaveTextContent(testTitle);
    expect(image).toHaveAttribute('src', expect.stringContaining(testUrl));
  });
});
