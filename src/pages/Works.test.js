import '@testing-library/jest-dom';
import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Works, { GET_WORKS, WorkItem, ControlButtons } from './Works';

describe('Works page test', () => {
  it('Works component test', async () => {
    const testTitle = 'Bu bir basliktir';
    const testText = 'Test content sahdhgas fasjdh sdjfhs.';
    const testUrl = '/git/yinegit.jpg';
    const testTitle2 = 'Bu bir basliktir2';
    const testText2 = 'Test content sahdhgas fasjdh sdjfhs.2';
    const testUrl2 = '/git/yinegit.jpeg';
    const mocks = [
      {
        request: {
          query: GET_WORKS,
        },
        result: {
          data: {
            works: [
              { id: '0', Content: { Title: testTitle, BodyText: testText, Image: { url: testUrl }, }, },
              { id: '1', Content: { Title: testTitle2, BodyText: testText2, Image: { url: testUrl2 }, }, }
            ],
          },
        },
      },
    ];

    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Works />
      </MockedProvider>
    );

    //expect(screen.queryByText('Loading')).toBeInTheDocument();

    const text = await screen.findByText(testText);
    const title = await screen.findByText(testTitle);
    const image = await screen.findByTestId('work-image');
    expect(text).toHaveTextContent(testText);
    expect(title).toHaveTextContent(testTitle);
    expect(image).toHaveAttribute('src', expect.stringContaining(testUrl));

    expect(screen.queryByText(testText2)).toBeNull();
    expect(screen.queryByText(testTitle2)).toBeNull();
    expect(screen.queryByText(testUrl2)).toBeNull();

    userEvent.click(screen.getByText(/next/i));
    expect(text).toHaveTextContent(testText2);
    expect(title).toHaveTextContent(testTitle2);
    expect(image).toHaveAttribute('src', expect.stringContaining(testUrl2));

    userEvent.click(screen.getByText(/prev/i));
    expect(text).toHaveTextContent(testText);
    expect(title).toHaveTextContent(testTitle);
    expect(image).toHaveAttribute('src', expect.stringContaining(testUrl));
  });

  it('WorkItem component test', async () => {
    const testTitle = 'Travolefjıjfeıj';
    const testText = 'Test content, content mi dedim hayır. Aahdhgas fasjdh sdjfhs.';
    const testUrl = '/git/busefergitme.jpg';
    const data = { id: '0', Content: { Title: testTitle, BodyText: testText, Image: { url: testUrl }, }, };

    render(
      <WorkItem data={data} />
    );

    const image = await screen.findByTestId('work-image');

    expect(screen.queryByText(testText)).toBeInTheDocument();
    expect(screen.queryByText(testTitle)).toBeInTheDocument();
    expect(image).toHaveAttribute('src', expect.stringContaining(testUrl));
  });
});
