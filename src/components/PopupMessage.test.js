import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import PopupMessage from './PopupMessage';

describe('PopupMessage component test', () => {

  it('Message test = success, error, null', () => {
    let testMessage = 'Your message has been sent';
    const { rerender } = render(<PopupMessage message={testMessage} />);

    expect(screen.queryByText(testMessage)).toBeInTheDocument();

    testMessage = 'Your message could not be sent';
    rerender(<PopupMessage message={testMessage} />);
    expect(screen.queryByText(testMessage)).toBeInTheDocument();

    rerender(<PopupMessage message="" />);
    expect(screen.queryByText(testMessage)).toBeNull();
  });

});
