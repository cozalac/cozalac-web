import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import Cube from './Cube';

describe('Cube component test', () => {
  it('Cube test', () => {
    const cubeFaces = ['önüm', 'arkam', 'sağım', 'solum', 'sobe', 'eee'];
    const showFace = 'sağım';
    render(<Cube cubeFaces={cubeFaces} show={showFace} />);
    expect(screen.queryByText(showFace)).toBeInTheDocument();
    /*expect(screen.getByText('solum').parentElement).toBeVisible();*/
  });
});
