import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ContactForm from './ContactForm';

describe('ContactForm component test', () => {
  it('ContactForm test', async () => {
    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({}),
      })
    );

    render(<ContactForm />);
    userEvent.paste(screen.getByLabelText(/Name:/i), 'Kollum');
    userEvent.paste(screen.getByLabelText(/Mail:/i), 'test@test.com');
    userEvent.paste(screen.getByLabelText(/Subject:/i), 'Kolsuzum');
    userEvent.paste(screen.getByLabelText(/Message:/i), 'This is test message. Deneme. Test, test midir?');
    userEvent.click(screen.getByText(/send/i));

    const alert = await screen.findByRole('alert');

    expect(alert).toHaveTextContent(/Your message has been sent/i);
  });
});
