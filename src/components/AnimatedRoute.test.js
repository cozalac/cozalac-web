import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { renderHook, act } from '@testing-library/react-hooks';
import { useLocation } from 'wouter';
import AnimatedRoute from './AnimatedRoute';

describe('AnimatedRoute component tests', () => {
  const { result, unmount } = renderHook(() => useLocation());
  const setLocation = result.current[1];

  it('AnimatedRoute test', async () => {
    const testText = 'testo';

    render(
      <AnimatedRoute path="/muck" type="pl">
        {testText}
      </AnimatedRoute>
    );

    expect(screen.queryByText(testText)).toBeNull();
    unmount();
  });

  it('AnimatedRoute change test', async () => {
    const testText = 'testo';

    act(() => setLocation('/muck'));

    render(
      <AnimatedRoute path="/muck" type="pl">
        {testText}
      </AnimatedRoute>
    );

    const tb = await screen.findByTestId('animated-route');

    expect(tb).toBeInTheDocument();
    expect(tb).toHaveClass('pl', 'pl-appear');

    await new Promise((r) => setTimeout(r, 500));
    expect(tb).toHaveClass('pl', 'pl-appear-done', 'pl-enter-done');

    unmount();
  });
});
