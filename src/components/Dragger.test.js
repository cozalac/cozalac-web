import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import DraggableComp from './Dragger';

/* Empty test */
describe('App component tests', () => {
  it('renders learn react link', () => {
    let testPrev = '/';
    let testCurrent = '/works';
    let testNext = '/about';
    render(
      <DraggableComp prev={testPrev} next={testNext} current={testCurrent}>
        <div>Test test</div>
      </DraggableComp>
    );
  });
});
